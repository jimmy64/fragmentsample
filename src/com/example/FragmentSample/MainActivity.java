package com.example.FragmentSample;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {
    private FragmentManager fragmentManger;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        fragmentManger=getFragmentManager();
        getActionBar().hide();
        findViewById(R.id.goFragment1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragment(new Fragment1());
            }
        });
        findViewById(R.id.goFragment2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFragment(new Fragment2());
            }
        });
    }

    public void showFragment(Fragment fragment)
    {
        FragmentTransaction transcation= fragmentManger.beginTransaction() ;
        transcation.replace(android.R.id.content, fragment) ;
        transcation.addToBackStack("toMain");       // remove this line , when we want the back key exit activity directly
        transcation.commit() ;
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        getActionBar().show();
    }

    void fragementDetactch(Fragment fragment) {

        getActionBar().hide();
    }
}
